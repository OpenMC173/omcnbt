module omcnbt.tag;

import std.bitmanip;
import std.conv;
import std.string;

import omcnbt.util;


enum TagType : ubyte {
    TAG_End = 0,
    TAG_Byte,
    TAG_Short,
    TAG_Int,
    TAG_Long,
    TAG_Float,
    TAG_Double,
    TAG_Byte_Array,
    TAG_String,
    TAG_List,
    TAG_Compound,
    TAG_Int_Array,
    TAG_Long_Array,
}

abstract class NBTtag
{
    abstract string prettyPrint();

    abstract TagType type() @property const;

    ubyte[] toBinary() const
    {
        return cast(ubyte[]) [type()];
    }
}

abstract class NamedTag : NBTtag
{
    abstract string name() @property const;

    override ubyte[] toBinary() const
    {
        return
            super.toBinary() ~
            nativeToBigEndian(name().length.to!short) ~
            cast(ubyte[]) name();
    }
}

final class EndTag : NBTtag
{
    override TagType type() @property const { return TagType.TAG_End; }

    override string prettyPrint() {
        return "TAG_End";
    }

    override bool opEquals(Object o) const
    {
        return true;
    }

    override size_t toHash() const
    {
        return 0;
    }
}

final class PrimitiveTag(Payload) : NamedTag
if (is(Payload == byte) ||
    is(Payload == short) ||
    is(Payload == int) ||
    is(Payload == long) ||
    is(Payload == float) ||
    is(Payload == double))
{
    Payload data;

    private string _name;
    override string name() @property const { return _name; }

    this(Payload data, string name)
    {
        this.data = data;
        this._name = name;
    }

    override TagType type() @property const
    {
        static if (is(Payload == byte))
            return TagType.TAG_Byte;
        else static if (is(Payload == short))
            return TagType.TAG_Short;
        else static if (is(Payload == int))
            return TagType.TAG_Int;
        else static if (is(Payload == long))
            return TagType.TAG_Long;
        else static if (is(Payload == float))
            return TagType.TAG_Float;
        else static if (is(Payload == double))
            return TagType.TAG_Double;
    }

    override string prettyPrint() {
        string TAG_TYPE = "%s".format(type());
        return format(TAG_TYPE ~ "(\"%s\"): %s", name, data);
    }

    override ubyte[] toBinary() const
    {
        return
            super.toBinary() ~
            nativeToBigEndian(data);
    }

    override bool opEquals(Object o) const
    {
        const PrimitiveTag!Payload rhs = cast(PrimitiveTag!Payload) o;
        return data == rhs.data && name == rhs.name;
    }

    override size_t toHash() const
    {
        return hashOf(data);
    }
}

alias ByteTag = PrimitiveTag!byte;
alias ShortTag = PrimitiveTag!short;
alias IntTag = PrimitiveTag!int;
alias LongTag = PrimitiveTag!long;
alias FloatTag = PrimitiveTag!float;
alias DoubleTag = PrimitiveTag!double;

final class ByteArrayTag : NamedTag
{
    byte[] data;

    private string _name;
    override string name() @property const { return _name; }

    this(byte[] data, string name)
    {
        this.data = data;
        this._name = name;
    }

    override TagType type() @property const { return TagType.TAG_Byte_Array; }

    override string prettyPrint() {
        string s = format("TAG_Byte_Array(\"%s\"): [ ", name);
        string contents = "";
        foreach (e; data) {
            contents ~= "%d ".format(e);
        }
        s ~= contents/*.indent("    ")*/;
        s ~= "]";
        return s;
    }

    override ubyte[] toBinary() const
    {
        return
            super.toBinary() ~
            nativeToBigEndian(data.length.to!int) ~
            cast(ubyte[]) data;
    }

    override bool opEquals(Object o) const
    {
        const ByteArrayTag rhs = cast(ByteArrayTag) o;
        return data == rhs.data && name == rhs.name;
    }

    override size_t toHash() const
    {
        return hashOf(data);
    }
}

final class StringTag : NamedTag
{
    string data;

    private string _name;
    override string name() @property const { return _name; }

    this(string data, string name)
    {
        this.data = data;
        this._name = name;
    }

    override TagType type() @property const { return TagType.TAG_String; }

    override string prettyPrint() {
        return format("TAG_String(\"%s\"): \"%s\"", name, data);
    }

    override ubyte[] toBinary() const
    {
        return
            super.toBinary() ~
            nativeToBigEndian(data.length.to!short) ~
            cast(ubyte[]) data;
    }

    override bool opEquals(Object o) const
    {
        const StringTag rhs = cast(StringTag) o;
        return data == rhs.data && name == rhs.name;
    }

    override size_t toHash() const
    {
        return hashOf(data);
    }
}

final class ListTag(Payload) : NamedTag
if (is(Payload == byte) ||
    is(Payload == short) ||
    is(Payload == int) ||
    is(Payload == long) ||
    is(Payload == float) ||
    is(Payload == double) ||
    is(Payload == NBTtag[]))
{
    Payload[] tags;

    private string _name;
    override string name() @property const { return _name; }

    this(string name)
    {
        this._name = name;
    }

    override TagType type() @property const { return TagType.TAG_List; }

    override string prettyPrint()
    {
        string s = format("TAG_List(\"%s\"): [\n", name);
        string contents = "";
        foreach (tag; tags) {
            static if (is(Payload == NBTtag[]))
            {
                contents ~= "TAG_Compound(): [\n";
                string innerContents = "";
                foreach (innerTag; tag)
                {
                    innerContents ~= innerTag.prettyPrint();
                    innerContents ~= "\n";
                }

                contents ~= innerContents.indent("    ");
                contents ~= "]";
            }
            else
            {
                contents ~= "%s".format(tag);
            }
            contents ~= "\n";
        }
        s ~= contents.indent("    ");
        s ~= "]";
        return s;
    }

    override ubyte[] toBinary() const
    {
        static if (is(Payload == byte))
            TagType payloadType = TagType.TAG_Byte;
        else static if (is(Payload == short))
            TagType payloadType = TagType.TAG_Short;
        else static if (is(Payload == int))
            TagType payloadType = TagType.TAG_Int;
        else static if (is(Payload == long))
            TagType payloadType = TagType.TAG_Long;
        else static if (is(Payload == float))
            TagType payloadType = TagType.TAG_Float;
        else static if (is(Payload == double))
            TagType payloadType = TagType.TAG_Double;
        else static if (is(Payload == NBTtag[]))
            TagType payloadType = TagType.TAG_Compound;

        ubyte[] tagsBinary = [];

        static if (is(Payload == NBTtag[]))
        {
            // This is a list of compound tags

            foreach (compoundTag; tags)
            {
                foreach (subTag; compoundTag)
                {
                    tagsBinary ~= subTag.toBinary();
                }
                tagsBinary ~= new EndTag().toBinary();
            }
        }
        else
        {
            foreach (e; tags)
            {
                tagsBinary ~= nativeToBigEndian(e);
            }
        }

        return
            super.toBinary() ~
            cast(ubyte[]) [payloadType] ~
            (cast(ubyte[]) nativeToBigEndian(tags.length.to!int)) ~
            tagsBinary;
    }

    override bool opEquals(Object o) const
    {
        const ListTag!Payload rhs = cast(ListTag!Payload) o;
        return tags == rhs.tags && name == rhs.name;
    }

    override size_t toHash() const @trusted
    {
        size_t hash = 0;
        foreach (tag; tags)
        {
            static if (is(Payload == NBTtag[]))
            {
                foreach (subTag; tag)
                {
                    hash ^= typeid(subTag).getHash(&subTag);
                }
            }
            else
            {
                hash ^= hashOf(tag);
            }
        }
        return hash;
    }
}

final class CompoundTag : NamedTag
{
    NamedTag[] tags;

    private string _name;
    override string name() @property const { return _name; }

    this(string name)
    {
        this._name = name;
    }

    T getChild(T : NamedTag)(string name)
        if(!is(T == EndTag))
    {
        NamedTag found = null;
        foreach (tag; tags)
        {
            if (tag.name == name)
            {
                found = tag;
                break;
            }
        }
        if (cast(T) found)
            return cast(T) found;
        else
            return null;
    }

    override TagType type() @property const { return TagType.TAG_Compound; }

    override string prettyPrint()
    {
        string s = format("TAG_Compound(\"%s\"): [\n", name);
        string contents = "";
        foreach (e; tags) {
            contents ~= e.prettyPrint();
            contents ~= "\n";
        }
        s ~= contents.indent("    ");
        s ~= "]";
        return s;
    }

    override ubyte[] toBinary() const
    {
        ubyte[] tagsBinary = [];

        foreach (tag; tags)
        {
            tagsBinary ~= tag.toBinary();
        }

        return
            super.toBinary() ~
            tagsBinary ~
            new EndTag().toBinary();
    }

    override bool opEquals(Object o) const
    {
        const CompoundTag rhs = cast(CompoundTag) o;
        return tags == rhs.tags && name == rhs.name;
    }

    override size_t toHash() const @trusted
    {
        size_t hash = 0;
        foreach (tag; tags)
        {
            hash ^= typeid(tag).getHash(&tag);
        }
        return hash;
    }
}

final class IntArrayTag : NamedTag
{
    int[] data;

    private string _name;
    override string name() @property const { return _name; }

    this(int[] data, string name)
    {
        this.data = data;
        this._name = name;
    }

    override TagType type() @property const { return TagType.TAG_Int_Array; }

    override string prettyPrint() {
        string s = format("TAG_Int_Array(\"%s\"): [ ", name);
        string contents = "";
        foreach (e; data) {
            contents ~= "%d ".format(e);
        }
        s ~= contents/*.indent("    ")*/;
        s ~= "]";
        return s;
    }

    override ubyte[] toBinary() const
    {
        ubyte[] eltsBinary;

        foreach (e; data)
        {
            eltsBinary ~= nativeToBigEndian(e);
        }

        return
            super.toBinary() ~
            nativeToBigEndian(data.length.to!short) ~
            eltsBinary;
    }

    override bool opEquals(Object o) const
    {
        const IntArrayTag rhs = cast(IntArrayTag) o;
        return data == rhs.data && name == rhs.name;
    }

    override size_t toHash() const
    {
        return hashOf(data);
    }
}

final class LongArrayTag : NamedTag
{
    long[] data;

    private string _name;
    override string name() @property const { return _name; }

    this(long[] data, string name)
    {
        this.data = data;
        this._name = name;
    }

    override TagType type() @property const { return TagType.TAG_Long_Array; }

    override string prettyPrint() {
        string s = format("TAG_Long_Array(\"%s\"): [ ", name);
        string contents = "";
        foreach (e; data) {
            contents ~= "%d ".format(e);
        }
        s ~= contents/*.indent("    ")*/;
        s ~= "]";
        return s;
    }

    override ubyte[] toBinary() const
    {
        ubyte[] eltsBinary;

        foreach (e; data)
        {
            eltsBinary ~= nativeToBigEndian(e);
        }

        return
        super.toBinary() ~
        nativeToBigEndian(data.length.to!short) ~
        eltsBinary;
    }

    override bool opEquals(Object o) const
    {
        const LongArrayTag rhs = cast(LongArrayTag) o;
        return data == rhs.data && name == rhs.name;
    }

    override size_t toHash() const
    {
        return hashOf(data);
    }
}
