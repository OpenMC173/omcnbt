module omcnbt.test;

import std.conv;
import std.stdio;

import omcnbt.parse;
import omcnbt.tag;
import omcnbt.util;


package void expect(E, A)(const auto ref E expected, const auto ref A actual)
{
    import core.exception : AssertError;

    try
    {
        assert(actual == expected);
    }
    catch (AssertError e)
    {
        writeln("Assertion failed. Expected vs. Actual:\n");
        writeln("----------------------------------------");
        writeln(expected.to!string);
        writeln("----------------------------------------");
        writeln(actual.to!string);
        writeln("----------------------------------------");
        throw e;
    }
}

private unittest
{
    import std.stdio : writeln;

    ubyte[] data = cast(ubyte[]) hexString!"
    0A 0003 466F6F
        08 0004 74657374 000B 68656c6c6f20776f726c64
        02 0002 7368 0005
    00
    ";
    CompoundTag expected = new CompoundTag("Foo");
    expected.tags ~= new StringTag("hello world", "test");
    expected.tags ~= new ShortTag(5, "sh");
    NBTtag actual = parseNBT(data);
    expect(cast(NBTtag) expected, actual);
}

private unittest
{
    import std.stdio : writeln;

    ubyte[] data = cast(ubyte[]) hexString!"
0A 0003 466F6F
    08 0004 74657374 000B 68656c6c6f20776f726c64
    02 0002 7368 0005
00
    ";
    string expected =
`TAG_Compound("Foo"): [
    TAG_String("test"): "hello world"
    TAG_Short("sh"): 5
]`;
    string actual = parseNBT(data).prettyPrint();
    expect(expected, actual);
}

private unittest
{
    import std.stdio : writeln;

    ubyte[] data = cast(ubyte[]) hexString!"
    0A 0003 466F6F
        08 0004 74657374 000B 68656c6c6f20776f726c64
        02 0002 7368 0005
    00
    ";
    NBTtag tag = parseNBT(data);
    expect(data, tag.toBinary());
}
