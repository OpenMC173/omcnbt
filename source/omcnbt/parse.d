module omcnbt.parse;

import std.bitmanip;
import std.conv;
import std.digest;
import std.string;
import std.traits;

import omcnbt.tag;
import omcnbt.util;


T parseNBT(T : NBTtag = NBTtag)(ubyte[] data)
{
    uint consumed = 0;

    NBTtag tag = consumeTag(data, consumed);

    if (cast(T) tag)
    {
        return cast(T) tag;
    }

    static if (!is(T == NBTtag))
    {
        // The cast(Object) is necessary because typeid doesn't return the dynamic type
        // if the argument is an interface. casting to object is a dirty but effective workaround.
        // https://forum.dlang.org/thread/uoysajhpglfsrpssbgdt@forum.dlang.org
        throw new Exception("Root of NBT data was expected to be " ~
                            typeid(T).name ~ " but was " ~ typeid(cast(Object)tag).name);
    }
    else {
        throw new Exception("Should never be able to reach this point.");
    }
}

NBTtag consumeTag(ubyte[] data, ref uint consumed)
{
    immutable TagType type = consumeEnum!TagType(data, consumed);

    // TAG_End is not named, does not contain the extra 2 bytes, and the name is assumed to be empty.
    if (type == TagType.TAG_End) return new EndTag();

    immutable short nameLength = consumePrimitive!short(data, consumed);
    immutable string name = consumeString(data, nameLength, consumed);

    final switch(type)
    {
        case TagType.TAG_End: {
            throw new Error("Should never have reached this point.");
        }

        case TagType.TAG_Byte: {
            return new ByteTag(consumePrimitive!byte(data, consumed), name);
        }

        case TagType.TAG_Short: {
            return new ShortTag(consumePrimitive!short(data, consumed), name);
        }

        case TagType.TAG_Int: {
            return new IntTag(consumePrimitive!int(data, consumed), name);
        }

        case TagType.TAG_Long: {
            return new LongTag(consumePrimitive!long(data, consumed), name);
        }

        case TagType.TAG_Float: {
            return new FloatTag(consumePrimitive!float(data, consumed), name);
        }

        case TagType.TAG_Double: {
            return new DoubleTag(consumePrimitive!double(data, consumed), name);
        }

        case TagType.TAG_Byte_Array: {
            immutable int length = consumePrimitive!int(data, consumed);
            return new ByteArrayTag(consumeArray!byte(data, length, consumed), name);
        }

        case TagType.TAG_String: {
            immutable short length = consumePrimitive!short(data, consumed);
            return new StringTag(consumeString(data, length, consumed), name);
        }

        case TagType.TAG_List: {

            // TAG_Byte's payload tagId,
            // TAG_Int's payload size,
            // size tags' payloads, all of type tagId.
            immutable TagType eltType = consumeEnum!TagType(data, consumed);
            immutable int length = consumePrimitive!int(data, consumed);
            switch (eltType)
            {
                case TagType.TAG_End: {
                    throw new Exception("Malformed NBT: TAG_List has element type TAG_Empty.");
                }

                case TagType.TAG_Byte: {
                    return makeList!byte(data, consumed, length, name);
                }

                case TagType.TAG_Short: {
                    return makeList!short(data, consumed, length, name);
                }

                case TagType.TAG_Int: {
                    return makeList!int(data, consumed, length, name);
                }

                case TagType.TAG_Long: {
                    return makeList!long(data, consumed, length, name);
                }

                case TagType.TAG_Float: {
                    return makeList!float(data, consumed, length, name);
                }

                case TagType.TAG_Double: {
                    return makeList!double(data, consumed, length, name);
                }

                /*case TagType.TAG_String: {
                    // TODO: Support string lists
                    return ListTag!string.makeList(data, consumed, length, name);
                }*/

                case TagType.TAG_Compound: {
                    return makeList!(NBTtag[])(data, consumed, length, name);
                }

                default: {
                    throw new Exception("Unsupported: TAG_List with element type %s".format(eltType));
                }
            }
        }

        case TagType.TAG_Compound: {
            CompoundTag compound = new CompoundTag(name);
            while (true)
            {
                NBTtag next = consumeTag(data, consumed);
                if (cast(EndTag) next) break;
                compound.tags ~= cast(NamedTag) next;
            }
            return compound;
        }

        case TagType.TAG_Int_Array: {
            immutable int length = consumePrimitive!int(data, consumed);
            return new IntArrayTag(consumeArray!int(data, length, consumed), name);
        }

        case TagType.TAG_Long_Array: {
            immutable int length = consumePrimitive!int(data, consumed);
            return new LongArrayTag(consumeArray!long(data, length, consumed), name);
        }
    }
}

alias consumeEnum = consumePrimitive;

T consumePrimitive(T)(ubyte[] data, ref uint consumed)
if (isIntegral!T || isFloatingPoint!T)
{
    ubyte[T.sizeof] dataStatic = data[consumed .. consumed + T.sizeof];
    T current = bigEndianToNative!(T, T.sizeof)(dataStatic);
    consumed += T.sizeof;
    return current;
}

T[] consumeArray(T)(ubyte[] data, uint length, ref uint consumed)
if (isIntegral!T)
{
    immutable size_t numBytes = length * T.sizeof;
    T[] current = cast(T[]) data[consumed .. consumed + numBytes];
    consumed += numBytes;
    return current;
}

string consumeString(ubyte[] data, uint length, ref uint consumed)
{
    string current = assumeUTF(data[consumed .. consumed + length]).to!string;
    consumed += length;
    return current;
}

private ListTag!Payload makeList(Payload)(ubyte[] data, ref uint consumed, uint length, string name)
if (is(Payload == byte) ||
    is(Payload == short) ||
    is(Payload == int) ||
    is(Payload == long) ||
    is(Payload == float) ||
    is(Payload == double) ||
    is(Payload == NBTtag[]))
{
    static if (is(Payload == byte) ||
               is(Payload == short) ||
               is(Payload == int) ||
               is(Payload == long) ||
               is(Payload == float) ||
               is(Payload == double))
    {
        ListTag!Payload list = new ListTag!Payload(name);
        for (uint i = 0; i < length; i++)
        {
            list.tags ~= consumePrimitive!Payload(data, consumed);
        }
        return list;
    }
    else
    {
        ListTag!(NBTtag[]) list = new ListTag!(NBTtag[])(name);
        for (uint i = 0; i < length; i++)
        {
            NBTtag[] currentCompoundPayload;
            while (true)
            {
                NBTtag next = consumeTag(data, consumed);
                if (cast(EndTag) next) break;
                currentCompoundPayload ~= next;
            }
            list.tags ~= currentCompoundPayload;
        }
        return list;
    }
}
