module omcnbt.util;

import std.conv;
import std.file;
import std.path;
import std.stdio;

import omcnbt.test;


package string indent(string s, string indentation)
{
    import std.string : replace, endsWith;
    string temp = s;
    immutable bool hadTrailingNewline = endsWith(temp, "\n");
    bool hasTrailingNewline = hadTrailingNewline;
    while (hasTrailingNewline)
    {
        temp = temp[0 .. $ - 1];
        hasTrailingNewline = endsWith(temp, "\n");
    }
    string ret = indentation ~ temp;
    ret = ret.replace("\n", "\n" ~ indentation);
    if (hadTrailingNewline)
    {
        ret ~= "\n";
    }
    return ret;
}
unittest {
    string initial = "a\nb\nc";
    string actual = indent(initial, "    ");
    string expected = "    a\n    b\n    c";
    expect(expected, actual);
}

package string insertEveryNChars(string s, char toInsert, uint interval)
{
    string ret = "";
    uint numSoFar = 0;
    foreach (c; s)
    {
        if (numSoFar == interval)
        {
            ret ~= toInsert;
            numSoFar = 0;
        }
        ret ~= c;
        numSoFar++;
    }
    return ret;
}
