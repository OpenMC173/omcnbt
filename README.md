# OMCnbt

OMCnbt is a library for reading and writing NBT-formatted data.

## TODO

- Parse uncompressed NBT data
    - [x] `TAG_Byte`
    - [x] `TAG_Short`
    - [x] `TAG_Int`
    - [x] `TAG_Long`
    - [x] `TAG_Float`
    - [x] `TAG_Double`
    - [x] `TAG_ByteArray`
    - [x] `TAG_String`
    - `TAG_List` with element type: 
        - [x] `TAG_Byte`
        - [x] `TAG_Short`
        - [x] `TAG_Int`
        - [x] `TAG_Long`
        - [x] `TAG_Float`
        - [x] `TAG_Double`
        - [ ] `TAG_ByteArray`
        - [ ] `TAG_String`
        - [ ] `TAG_List`
        - [x] `TAG_Compound`
        - [ ] `TAG_IntArray`
        - [ ] `TAG_LongArray`
    - [x] `TAG_Compound`
    - [x] `TAG_IntArray`
    - [x] `TAG_LongArray`

- [x] Write structured data to NBT format.
- [x] Print parsed NBT data as human-readable string.
